﻿# Pianify
Pianify is an application whose 2 main goals are:
- transformation of an audio file to the MIDI format
- recognition of different instruments that play across a song

In its current stage, the second goal of the application is more of a proof of concept, as it only recognizes two instruments: piano and trumpet. 

# Installation

 1. Clone the repo: `git clone https://gitlab.com/soundwavers/pianify.git`
 2. Building the project requires CMake 3.11+ and a C++17 compatible compiler. For now, the project has only been successfully tested using MSVC 2017 and MSVC 2019.
 3. The actual project building is done using CMake from the GUI client or from the command line. After cloning the repository, enter the following commands in your preferred command line client (assuming that you have CMake in your PATH variable):

		 cd pianify
		 mkdir build
		 cd build  
		 cmake -A x64 ../
		 
 4. The project provides 2 options when building with CMake:
     - BUILD_GUI - as its name suggests, it refers to building the GUI. Build the user interface if you wish to test the project as is. If you want to embed it in your applications, you can safely disable BUILD_GUI.
     - USET_BOOST - there exists a client component of the project that was supposed to be used to fetch responses from the neural network. In some earlier stages of the project the neural network that recognizes instruments was written in Python and Keras, thus there was needed a communication bridge. This option is disabled by default as the client component is not used anymore.
 5.  Open the generated project files build it. It may take a while as the project has several dependencies.

**Notes:**
Building the GUI is available only on 64-bit architectures. If you build the project without the GUI then you can target both the x86 and x64 architectures.

# Dependencies
Pianify is written entirely in C++ and depends on the following libraries:

 - Aquila
 - tiny-dnn
 - Qt (optional)
 - QCustomPlot (optional)
 - midifile
 - Boost (optional)
